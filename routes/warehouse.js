const express = require("express");
const async = require("async");
const router = express.Router();
const _ = require("lodash");

const commonUtility = require("../common/commonUtility");
const user = require("../controllers/user");
const warehouse = require("../controllers/warehouse");
const files = require("../controllers/files");
const activityLog = require("../controllers/activityLogs");

const common = new commonUtility();
const warehouseController = new warehouse();
const filesController = new files();
const userController = new user();
const tasksController = new tasks();
const activityLogController = new activityLog();

router.post("/create/project", common.authorizeUser, handleCreateProject);
router.post("/project/update", common.authorizeUser, handleUpdateProject);
router.post("/list/warehouse", common.authorizeUser, handleListwarehouse);
router.post(
  "/list/recent/warehouse",
  common.authorizeUser,
  handleListRecentwarehouse
);
router.post("/project/by/id", common.authorizeUser, handleProjectById);
router.post(
  "/project/tasks/by/id",
  common.authorizeUser,
  handleProjectTasksById
);
router.get(
  "/project/get/all/team/members",
  common.authorizeUser,
  handleGetAllTeamMembers
);
router.post(
  "/project/add/team/member",
  common.authorizeUser,
  handleAddTeamMemberProject
);
router.post(
  "/project/remove/team/member",
  common.authorizeUser,
  handleRemoveTeamMember
);
router.get(
  "/project/delete/:projectId",
  common.authorizeUser,
  handleDeleteProject
);
router.post("/project/get/files", common.authorizeUser, getProjectFiles);

function handleCreateProject(req, res) {
  const userId = common.getUserId(req) || "";

  userController.findUserByUserId(
    common.castToObjectId(userId),
    common.getUserDetailsFields(),
    (err, existingUser) => {
      if (err || !existingUser) {
        return common.sendErrorResponse(res, "Error getting user details");
      }

      templatesController.getTemplateById(
        res,
        req.body.templateId,
        (template) => {
          template.sections.forEach((section) => {
            delete section.tasks;
          });


        }
      );
    }
  );
}

function handleUpdateProject(req, res) {
  const userId = common.getUserId(req) || "";

  userController.findUserByUserId(
    common.castToObjectId(userId),
    common.getUserDetailsFields(),
    (err, existingUser) => {
      if (err || !existingUser) {
        return common.sendErrorResponse(res, "Error getting user details");
      }

      warehouseController.updateProject(
        req,
        res,
        existingUser,
        (projectErr, updateProject) => {
          if (projectErr || !updateProject) {
            return common.sendErrorResponse(
              res,
              "Error in updating new project"
            );
          }

          res.send({
            msg: "Project updated successfully",
          });
          // activityLogController.insertLogs({}, savedProject._id, 'project', 'create', existingUser)
        }
      );
    }
  );
}

function handleGetAllTeamMembers(req, res) {
  const projectId = req.query.projectId;

  if (!common.isObjectId(projectId)) {
    return common.sendErrorResponse(
      res,
      "Please specifiy the valid project id"
    );
  }

  warehouseController.getAllTeamMembers(
    projectId,
    (teamMembersErr, teamMembers) => {
      if (teamMembersErr || !teamMembers) {
        return res.send({
          msg: "Error in getting team members",
          teamMembers: [],
        });
      }

      teamMembers = teamMembers || {} ? teamMembers.teamMembers : [];

      teamMembers.forEach((member) => {
        member.fullName = member.firstName + " " + member.lastName;
      });

      return res.send({
        msg: "Successfully got team members",
        teamMembers: teamMembers,
      });
    }
  );
}

function handleDeleteProject(req, res) {
  const userId = common.getUserId(req) || "";

  userController.findUserByUserId(
    common.castToObjectId(userId),
    { emailId: 1 },
    (err, existingUser) => {
      if (err || !existingUser) {
        return common.sendErrorResponse(res, "Error getting user details");
      }

      warehouseController.deleteProject(req, res, existingUser);
    }
  );
}

function getProjectFiles(req, res) {
  const projectId = req.body.projectId;

  if (!common.isObjectId(projectId)) {
    return common.sendErrorResponse(
      res,
      "Please specifiy the valid project id"
    );
  }

  warehouseController.getProjectFiles(projectId, (projectErr, warehouse) => {
    if (projectErr || !warehouse) {
      return common.sendErrorResponse(
        res,
        "Error in getting project files list"
      );
    }

    const sectionIds = [];
    const fileDirColor = "#555";
    const filesHierarchy = [];
    warehouse.sections = warehouse.sections || [];

    warehouse.sections.forEach((section) => {
      sectionIds.push(section._id);
      filesHierarchy.push({
        data: {
          sectionId: section._id,
          name: section.name,
          files: 0,
          kind: "dir",
          color: fileDirColor,
        },
        children: [],
      });
    });

    if (sectionIds.length > 0) {
      const query = {
        projectId: projectId,
        sectionId: { $in: sectionIds },
      };
      const projection = {
        _id: 1,
        type: 1,
        fileName: 1,
        uniqFileName: 1,
        sectionId: 1,
      };

      filesController.getFilesByQuery(query, projection, (files) => {
        const filesBySectionId = _.groupBy(files, (file) => file.sectionId);

        filesHierarchy.forEach((file) => {
          const selectedFile = filesBySectionId[file.data.sectionId];

          if (selectedFile) {
            file["data"]["files"] = selectedFile.length || 0;

            selectedFile.forEach((el) => {
              file["children"].push({
                data: {
                  fileId: el._id,
                  name: el.fileName,
                  kind: el.type,
                  link: el.uniqFileName,
                },
              });
            });
          }
        });

        res.send({
          msg: "Successfully got files",
          files: filesHierarchy,
        });
      });
    }
  });
}

function handleProjectById(req, res) {
  const projectId = req.body.projectId;
  const projection = req.body.projection || {};

  if (!common.isObjectId(projectId)) {
    return common.sendErrorResponse(
      res,
      "Please specifiy the valid project id"
    );
  }

  warehouseController.getProjectById(
    projectId,
    projection,
    (projectErr, project) => {
      if (projectErr || !project) {
        return common.sendErrorResponse(res, "Error in getting project");
      }

      res.send({
        msg: "Successfully got the project",
        project: project,
      });
    }
  );
}

function handleProjectTasksById(req, res) {
  const projectId = req.body.projectId;
  const projectProjection = req.body.projectProjection || {};
  const taskProjection = req.body.taskProjection || {};

  if (!common.isObjectId(projectId)) {
    return common.sendErrorResponse(
      res,
      "Please specifiy the valid project id"
    );
  }

  async.parallel(
    {
      project: function (callback) {
        warehouseController.getProjectById(
          projectId,
          projectProjection,
          (projectErr, project) => {
            if (projectErr || !project) {
              callback(null, {});
            } else {
              callback(null, project);
            }
          }
        );
      },
      tasks: function (callback) {
        tasksController.getTasksByProjectId(
          projectId,
          taskProjection,
          (tasksErr, tasks) => {
            if (tasksErr || !tasks) {
              callback(null, []);
            }

            callback(null, tasks);
          }
        );
      },
    },
    function (asyncErr, result) {
      const project = result.project || {};
      const tasks = result.tasks || [];
      const sectionWithTasks = {};

      tasks.forEach((task) => {
        if (!sectionWithTasks[task.sectionId]) {
          sectionWithTasks[task.sectionId] = [task];
        } else {
          sectionWithTasks[task.sectionId].push(task);
        }
      });

      if (project.sections) {
        project.sections.forEach((section) => {
          section.isActive = true;
          section.tasks = sectionWithTasks[section._id] || [];
        });
      }

      res.send({
        project: project,
        tasks: tasks,
      });
    }
  );
}

function handleListwarehouse(req, res) {
  const userId = common.getUserId(req) || "";

  userController.findUserByUserId(
    common.castToObjectId(userId),
    { emailId: 1 },
    (err, existingUser) => {
      if (err || !existingUser) {
        return common.sendErrorResponse(res, "Error getting user details");
      }

      warehouseController.listwarehouse(req, res, existingUser, (projectList) => {
        if (projectList && projectList[0]) {
          projectList.forEach(function (el) {
            el.img = common.generateImg(el.warehouseName);
          });
        }

        res.send({
          msg: "Successfully got project list",
          warehouse: projectList,
        });
      });
    }
  );
}

function handleListRecentwarehouse(req, res) {
  const userId = common.getUserId(req) || "";

  userController.findUserByUserId(
    common.castToObjectId(userId),
    { emailId: 1 },
    (err, existingUser) => {
      if (err || !existingUser) {
        return common.sendErrorResponse(res, "Error getting user details");
      }

      warehouseController.listRecentwarehouse(
        req,
        res,
        existingUser,
        (projectList) => {
          projectList = projectList || [];
          const warehouse = [];

          projectList.forEach((el) => {
            warehouse.push({
              members: el.teamMembers.length + " members",
              name: el.warehouseName,
              link: "/user-dashboard/list/" + el.teamId + "/" + el._id,
              teamId: el.teamId,
              city: el.city,
              _id: el._id,
              img: common.generateImg(el.warehouseName),
            });
          });

          res.send({
            msg: "Successfully got project list",
            warehouse: warehouse,
          });
        }
      );
    }
  );
}

function handleAddTeamMemberProject(req, res) {
  const userId = common.getUserId(req) || "";

  userController.findUserByUserId(
    common.castToObjectId(userId),
    { emailId: 1 },
    (err, existingUser) => {
      if (err || !existingUser) {
        return common.sendErrorResponse(res, "Error getting user details");
      }

      warehouseController.addTeamMember(req, res, existingUser);
    }
  );
}

function handleRemoveTeamMember(req, res) {
  const userId = common.getUserId(req) || "";

  userController.findUserByUserId(
    common.castToObjectId(userId),
    { emailId: 1 },
    (err, existingUser) => {
      if (err || !existingUser) {
        return common.sendErrorResponse(res, "Error getting user details");
      }

      warehouseController.removeTeamMember(req, res, existingUser);
    }
  );
}

module.exports = router;
