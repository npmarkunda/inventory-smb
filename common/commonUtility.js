const jwt = require("jwt-simple");
const validator = require("validator");
const mongoose = require("mongoose");

const JWT_TOKEN_SECRET = "const-@-oasis-@";

function CommonUtility() {}

CommonUtility.prototype.authorizeUser = (req, res, next) => {
  const common = new CommonUtility();

  if (req.headers.authorization) {
    const userId = decodeToken(extractToken(req), JWT_TOKEN_SECRET);
    if (validator.isAlphanumeric(userId)) {
      return next();
    } else {
      return common.sendErrorResponse(res, "Invalid token");
    }
  } else {
    return common.sendErrorResponse(
      res,
      "Token is absent in header. Please login"
    );
  }
};

CommonUtility.prototype.generateToken = function (userId) {
  return jwt.encode({ id: userId }, JWT_TOKEN_SECRET);
};

CommonUtility.prototype.getUserId = function (req) {
  return decodeToken(extractToken(req));
};

CommonUtility.prototype.sendErrorResponse = function (res, msg) {
  res.status(400);
  return res.send({ msg: msg });
};

CommonUtility.prototype.composeUserLoginCredentials = function (user) {
  return {
    firstName: user.firstName,
    lastName: user.lastName,
    emailId: user.emailId,
    token: new CommonUtility().generateToken(user._id),
  };
};

CommonUtility.prototype.castToObjectId = (id) => {
  return mongoose.Types.ObjectId(id);
};

CommonUtility.prototype.isObjectId = (id) => {
  if (!new CommonUtility().validateString(id)) {
    return false;
  }

  return mongoose.isValidObjectId(id);
};

CommonUtility.prototype.validateString = (data) => {
  if (data == "" || data == null || data == undefined || data == "undefined") {
    return false;
  }

  return true;
};

CommonUtility.prototype.generateImg = (name, len) => {
  var colorArray = [
    "FF6633",
    "FFB399",
    "FF33FF",
    "FFFF99",
    "00B3E6",
    "E6B333",
    "3366E6",
    "99FF99",
    "B34D4D",
    "80B300",
    "E6B3B3",
    "FF99E6",
    "CCFF1A",
    "FF1A66",
    "E6331A",
    "33FFCC",
    "66994D",
    "B366CC",
    "4D8000",
    "B33300",
    "CC80CC",
    "66664D",
    "991AFF",
    "E666FF",
    "4DB3FF",
    "1AB399",
    "E666B3",
    "33991A",
    "CC9999",
    "B3B31A",
    "00E680",
    "4D8066",
    "E6FF80",
    "1AFF33",
    "FF3380",
    "CCCC00",
    "4D80CC",
    "E64D66",
    "4DB380",
    "FF4D4D",
  ];

  len = len ? len : 1;

  if (len > 1) {
    name = name;
  } else {
    name = name.charAt(0);
  }

  var result = "";
  for (var i = 0; i < name.length; i++) {
    var code = name.toUpperCase().charCodeAt(i);
    if (code > 64 && code < 91) result += code - 64 + " ";
  }

  let hexColor = colorArray[result.slice(0, result.length - 1)];

  return (
    "https://ui-avatars.com/api/?name=" +
    name +
    "&length=" +
    len +
    "&color=fff" +
    "&font-size=.5" +
    "&background=" +
    hexColor
  );
};

CommonUtility.prototype.getUserDetailsFields = () => {
  return {
    _id: 1,
    firstName: 1,
    lastName: 1,
    designation: 1,
    city: 1,
    fullName: 1,
    emailId: 1,
    mobileNumber: 1,
    profilePicUrl: 1,
  };
};

function decodeToken(token) {
  try {
    return jwt.decode(token, JWT_TOKEN_SECRET).id;
  } catch (e) {
    return null;
  }
}

function extractToken(req) {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    return req.headers.authorization.split(" ")[1];
  }

  return null;
}

module.exports = CommonUtility;
